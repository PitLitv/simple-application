function SendFormData(){};
SendFormData.prototype = {
    $url: null,
    $nodeForm: null,
    $nodeMessage: null,
    $dataObject: null,
    $responseData: null,
    $nodeTable: null,
    ajaxSend: function () {
        this.setLocation(this.$url);
        this.getData();
        $.ajax({
            url: this.$url ,
            type: 'post',
            dataType: 'json',
            data: this.$dataObject
        }).done(function(response) {
            var node = $(SendFormData.prototype.$nodeMessage);
                node.html('');
            if(node.hasClass('success') || node.hasClass('error'))
                node.removeClass('success').removeClass('error');

            if($(SendFormData.prototype.$nodeTable).length)
                $(SendFormData.prototype.$nodeTable).html('');

            if(response) {
                if(response.status == 0) {
                    node.html(response.data).removeClass('hidden').addClass('success');
                } else if (response.status == 1){
                    node.html(response.data).removeClass('hidden').addClass('error');
                } else if (response.status == 4){
                    SendFormData.prototype.$responseData = response.data;
                    SendFormData.prototype.generateRows();
                }
            }
        });

    },
    getData: function() {
        this.$dataObject =  this.$nodeForm.serializeArray();
        this.$dataObject.push({ name : 'isAjax', value : 1 });
    },
    generateRows: function() {
        if(typeof this.$responseData == 'object') {
            for ($tr in this.$responseData) {
                var tr = $('<tr/>', {});
                    for($td in this.$responseData[$tr]) {
                        var text = this.$responseData[$tr][$td];
                        if($td == 'access_is_granted')
                            text = this.$responseData[$tr][$td] != null ? 1 : 0;
                        var td = $('<td/>', {
                            'class': '',
                            'text': text
                        });
                        tr.append(td);
                    }
                $(this.$nodeTable).append(tr);
            }
        }
    },
    setLocation: function() {
        if(this.$url === '') {
            this.$url = '/';
        }
        try {
            history.pushState(null, null,this.$url);
            return;
        } catch(e) {}
        location.hash = '#' + this.$url;
    }
};


