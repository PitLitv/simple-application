<?php
/**
 * Created by PhpStorm.
 * User: R1sen
 * Date: 05.11.2017
 * Time: 16:55
 */
define('DS',DIRECTORY_SEPARATOR);
define('ROOT', dirname(dirname(__FILE__)).DS);
define('LIBRARY', ROOT.'library'.DS);
define('APPLICATION', ROOT.'application'.DS);
define('CORE', APPLICATION.DS.'Core'.DS);
define('CONTROLLERS', APPLICATION.'Controllers'.DS);
define('MODELS', APPLICATION.'Models'.DS);
define('VIEWS', APPLICATION.'Views'.DS);
define('LAYOUTS', APPLICATION.'layouts'.DS);
require_once LIBRARY.'autoloader.php';

$application  = new Application();
$application->run();