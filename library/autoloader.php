<?php
require_once APPLICATION.'config'.DS.'config.php';

function __autoload($className) {

    $pathToCore= CORE.$className.'.php';
    $pathToLibrary = LIBRARY.$className.'.php';
    $pathToController = CONTROLLERS.$className.'.php';
    $pathToModel = MODELS.$className.'.php';

    if(file_exists($pathToCore)) {
        require_once $pathToCore;
    } elseif (file_exists($pathToLibrary)) {
        require_once $pathToLibrary;
    } elseif (file_exists($pathToController)) {
        require_once $pathToController;
    } elseif(file_exists($pathToModel)) {
        require_once $pathToModel;
    } else {
        throw new Exception('Falled to include class: '.$className);
    }
}