<?php

class Helper
{
    public static function toTranslit($str) {
        $tr = array(
            "А"=>"a","Б"=>"b","В"=>"v","Г"=>"g",
            "Д"=>"d","Е"=>"e","Ё"=>"e","Ж"=>"j","З"=>"z","И"=>"i","І"=>"i","Ї"=>"i",
            "Й"=>"y","К"=>"k","Л"=>"l","М"=>"m","Н"=>"n",
            "О"=>"o","П"=>"p","Р"=>"r","С"=>"s","Т"=>"t",
            "У"=>"u","Ф"=>"f","Х"=>"h","Ц"=>"ts","Ч"=>"ch",
            "Ш"=>"sh","Щ"=>"sch","Ъ"=>"","Ы"=>"y","Ь"=>"",
            "Э"=>"e","Є"=>"e","Ю"=>"yu","Я"=>"ya","а"=>"a","б"=>"b",
            "в"=>"v","г"=>"g","д"=>"d","е"=>"e","ж"=>"j",
            "з"=>"z","и"=>"i","і"=>"i","ї"=>"i","й"=>"y","к"=>"k","л"=>"l",
            "м"=>"m","н"=>"n","о"=>"o","п"=>"p","р"=>"r",
            "с"=>"s","т"=>"t","у"=>"u","ф"=>"f","х"=>"h",
            "ц"=>"ts","ч"=>"ch","ш"=>"sh","щ"=>"sch","ъ"=>"",
            "ы"=>"y","ь"=>"","э"=>"e","є"=>"e","ю"=>"yu","я"=>"ya",
            "ё"=>"e",
            " "=> "-", "."=> "", "/"=> "_"
        );
        return strtr($str,$tr);
    }

    public static function existsFile($path) {

        if(!file_exists($path)) {
            throw new Exception('File is not found in path: '.$path);
        }
        return $path;
    }

    public static function getErrorMessage($key) {
        $messages = [
            '0'=>'success',
            '1'=>'error',
            '4'=>'success get users',
            'EmailIsNotValid'=>'Вы ввели не правленый Email',
            'EmailIsValid'=>'Вы ввели корректный Email',
            'DateIsNotValid'=>'Вы ввели дату в неверном формате',
            'CardNumberIsNotValid'=>'Вы ввели номер карты в неверном формате',
            'IncorrectData'=>'Не верно введены данные',
            'JsonIsNotValid'=>'Ошибка при декодировании строки в JSON объект',
            'IsEmptyData'=>'Запрос не дал никаких результатов, повторите попытку',
        ];
        return isset($messages[$key]) ? $messages[$key] : $key;
    }

    public static function encodeJson($data, $status = false,$additionally = null) {

        if($status === false) {
            return json_encode($data);
        }

        if(!is_null($additionally)) {
            $result['additionally'] = $additionally;
        }
        $result['data'] = $data;
        $result['status'] = $status;
        $result['message'] = self::getErrorMessage($status);

        $json = json_encode($result);
        if(!self::isJson())
            $json = false;

        return $json;
    }

    public static function decodeJson($json,$array = false) {
        $json = json_decode($json,$array);
        if(!self::isJson())
            $json = false;

        return $json;
    }

    public static function isJson() {
        return JSON_ERROR_NONE == json_last_error() ? true : false;
    }

    public static function encodeRsa($data) {
        openssl_public_encrypt($data, $encrypted,  file_get_contents(Config::get('PUBLIC_KEY')));
        return $encrypted;
    }

    public static function decodeRsa($encrypted) {
        openssl_private_decrypt($encrypted, $decrypted,  file_get_contents(Config::get('PRIVATE_KEY')));
        return $decrypted;
    }

    public static function debug($variable, $varDump = false, $die = false, $return = false) {
        ob_start();

        if($varDump) {
            echo "<pre>";
            var_dump($variable);
            echo "</pre>";
        } else {
            echo "<pre>";
            print_r($variable);
            echo "</pre>";
        }

        $result =  ob_get_clean();

        if($return) {
            return $result;
        }

        if($die) {
            die($result);
        } else {
            echo $result;
        }
        return false;
    }
}

