<?php
class Model
{
    protected $db;

    public  function  __construct() {
        $this->db = Application::$db;
    }

    public function getListInfo() {
        $sql = "select `alias`,`title` from informations";
        $result = $this->db->query($sql);
        return isset($result[0]) ? $result : null;
    }
}