<?php
class Router
{

    protected  $uri;
    protected  $controller;
    protected  $action;
    protected  $params;

    public  $route;

    public function getUri()  {
        return $this->uri;
    }

    public function getController() {
        return $this->controller;
    }

    public function getAction() {
        return $this->action;
    }

    public function getParams() {
        return $this->params;
    }

    public function getPost() {
        return $_POST;
    }

    public function getRoute() {
        return $this->route;
    }

    public function __construct($uri) {
        $this->uri = urldecode(trim($uri, '/'));
        $routes = Config::get('routes');
        $this->route = Config::get('default_route');
        $this->controller = Config::get('default_controller');
        $this->action = Config::get('default_action');

        $uriParts = explode('?', $this->uri);
        $path = $uriParts[0];
        $pathParts = explode('/',$path);

        if(count($pathParts)) {
            if(in_array(strtolower(current($pathParts)), array_keys($routes))) {
                $this->route = strtolower(current($pathParts));
                array_shift($pathParts);
            }

            if(current($pathParts) && file_exists(CONTROLLERS.current($pathParts).'_Controller.php') ) {
                $this->controller = strtolower(current($pathParts));
                array_shift($pathParts);
            }

            if(current($pathParts)) {
                $this->action = strtolower(current($pathParts));
                array_shift($pathParts);
            }
            $params = [];
            if(isset($uriParts[1]) && !empty($uriParts[1]) ) {
                parse_str($uriParts[1], $params);
            }
            $this->params = $pathParts;
        }

    }

    public static function  redirect($location) {
        header("Location: $location");
    }


}