<?php
class View
{
    protected $layoutPath = '';
    protected $layout = '';
    protected $scriptPath = '';
    protected $appendScripts = '';
    protected $appendFiles = '';

    public function __construct() {
        $this->getDefaultViewPath();
    }

    protected function getDefaultViewPath() {
        $router = Application::getRouter();
        if(!$router) {
            throw new  Exception('Method static class failed'.$router);
        }
        $layout = $router->getRoute();
        $controllerDir = $router->getController();
        $templateName = $router->getAction();
        $this->setRender(VIEWS.ucfirst($controllerDir).DS.$templateName);
        $this->setLayout(LAYOUTS.$layout);
    }

    public function setLayout($path) {
        $path = $path.'.phtml';
        $this->layoutPath = Helper::existsFile($path);
    }

    public function setRender($path) {
        $path = $path.'.phtml';
        $this->scriptPath = Helper::existsFile($path);
    }

    public function setNoRender() {
        $this->scriptPath = false;
    }

    public function renderScript() {
        ob_start();
        include(Helper::existsFile($this->scriptPath));
        $content = ob_get_clean();
        return $content;
    }

    public function appendScript($script) {
        $this->appendScripts .= $script;
    }

    public function getAppendScripts() {
        return $this->appendScripts;
    }

    public function getAppendFiles() {
        return $this->appendFiles;
    }

    public function appendFile($script) {
        $this->appendFiles .= "<script src='$script'></script>";
    }

    public function render() {
        if($this->scriptPath)
            $this->layout = $this->renderScript();
        ob_start();
        include(Helper::existsFile($this->layoutPath));
        $content = ob_get_clean();
        return $content;
    }

    protected function convertToObject($array) {
        if(is_array($array)) {
            return (object) $array;
        } else {
            return $array;
        }
    }

}