<?php
class Application
{
    protected static $router;

    public $uri;

    public static $db;

    public function __construct() {
        $this->uri = $_SERVER['REQUEST_URI'];
    }

    public static function getRouter() {
        return self::$router;
    }

    public function run() {
        self::$router = new Router($this->uri);

        self::$db = new DB (Config::get('db.host'), Config::get('db.user'), Config::get('db.password'), Config::get('db.db_name'));

        $controllerClass = ucfirst(self::$router->getController()).'_Controller';
        $controllerMethod = strtolower(self::$router->getAction()).'Action';
        $controllerObject = new $controllerClass();

        if(method_exists($controllerObject, $controllerMethod)) {
            $view = new View();
            $controllerObject->setView($view);
            $controllerObject->$controllerMethod();
        } else {
            throw new Exception('Method ' . $controllerMethod . ' of class ' . $controllerClass . ' does not exist');
        }
        echo $view->render();
    }
}