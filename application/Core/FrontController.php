<?php
class FrontController
{
    protected $content;

    protected $model;

    protected $params;

    protected $request;

    protected $view;

    public function getModel() {
        return $this->model;
    }

    public function getParams() {
        return $this->params;
    }

    public function getSideBar() {
        $model = new Model();
        return array(
            'left_info_alias' => $model->getListInfo()
        );
    }

    public function setView(&$view) {
        $this->view = $view;
    }

    public function __construct() {
        $this->params = Application::getRouter()->getParams();
        $this->request = Application::getRouter()->getPost();
    }
}