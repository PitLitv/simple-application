<?php
class DB
{
    protected $connection;

    protected $host = '';
    protected $user = '';
    protected $password = '';
    protected $dataBaseName = '';

    public function __construct($host, $user, $password, $dataBaseName) {
        $this->host = $host;
        $this->user = $user;
        $this->password = $password;
        $this->dataBaseName = $dataBaseName;
    }

    public function query($query) {

        $config = "host=$this->host port=5432 dbname=$this->dataBaseName user=$this->user";
        $connect = pg_connect($config);

        if(pg_last_error($connect)) {
            throw new Exception('Could not connect: '.pg_last_error($connect));
        }

        $result = pg_query($query);

        if(pg_last_error($connect)) {
            throw new Exception('You have syntax error: ' . pg_last_error());
        }

        if (is_bool($result)) {
            return $result;
        }

        $data = array();
        while ($row = pg_fetch_assoc($result)) {
            $data[] = $row;
         }
        pg_close($connect);
        return $data;
    }

    public  function escape($str) {
        return pg_escape_string($str);
    }

    public function prepareAlias($str) {
        $alias = preg_replace("#[^\d\w- ]+#ui","",trim(strip_tags($str)));
        $alias = preg_replace("#[\s]+#ui"," ",$alias);
        $alias = str_replace("__","_",$alias);
        $alias = str_replace("--","-",$alias);
        $alias = strtolower($alias);

        return Helper::toTranslit($alias);
    }
}