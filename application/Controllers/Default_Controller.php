<?php
class Default_Controller extends FrontController
{
    private $handlerModel;

    public function __construct() {
        parent::__construct();
        $this->handlerModel = new Handler_Model();
        $this->defaultModel = new Default_Model();
    }

    public function indexAction() {
        $this->view->header = 'Перейдите на нужный вам раздел';
        $this->view->title = 'Simple Application ';
    }

    public function dateAction() {
        $this->view->header = 'Форматирование даты';
        $this->view->title = 'Simple Application';

        if(isset($this->request['isAjax']) && isset($this->request['date'])) {
            $dateValidate = $this->handlerModel->dateFormat($this->request['date']);

            if($dateValidate) {
                exit(Helper::encodeJson($dateValidate,0));
            } else {
                $message = Helper::getErrorMessage('DateIsNotValid');
                exit(Helper::encodeJson($message,1));
            }
        }
    }

    public function cardAction() {
        $this->view->header = 'Платежная карта';
        $this->view->title = 'Simple Application';

        if(isset($this->request['isAjax']) && isset($this->request['card']) && is_array($this->request['card']) ) {
            $result = $this->handlerModel->compileCardNumber($this->request['card']);

            if($result) {
                $message = Helper::getErrorMessage($result);
                exit(Helper::encodeJson($message,0));
            } else {
                $message = Helper::getErrorMessage('CardNumberIsNotValid');
                exit(Helper::encodeJson($message,1));
            }
        }
    }

    public function rsaAction() {
        $this->view->header = 'Шифрование Json публичным RSA ключем';
        $this->view->title = 'Simple Application';

        if(isset($this->request['isAjax'])) {
            if(isset($this->request['json']) && Helper::decodeJson($this->request['json'])) {
                $result = $this->handlerModel->convertRsaToArray(Helper::encodeRsa($this->request['json']));
                $result = Helper::debug($result,false,false,true);
                exit(Helper::encodeJson($result,0));
            } else {
                $message = Helper::getErrorMessage('JsonIsNotValid');
                exit(Helper::encodeJson($message,1));
            }
        }
    }

    public function emailAction() {
        $this->view->header = 'Валидация email';
        $this->view->title = 'Simple Application';

        if(isset($this->request['isAjax']) && isset($this->request['email'])) {
            $emailValidate = $this->handlerModel->validateEmail($this->request['email']);

            if($emailValidate) {
                $message = Helper::getErrorMessage('EmailIsValid');
                exit(Helper::encodeJson($message,0));
            } else {
                $message = Helper::getErrorMessage('EmailIsNotValid');
                exit(Helper::encodeJson($message,1));
            }
        }
    }

    public function exponentiationAction() {
        $this->view->header = 'Возведение в степень';
        $this->view->title = 'Simple Application';

        if(isset($this->request['isAjax']) && isset($this->request['value']) && isset($this->request['count']) && $this->request['value'] > 0) {
            $result = $this->handlerModel->exponentiation((int)$this->request['value'],(int)$this->request['count'],(int)$this->request['value']);

            if($result) {
                exit(Helper::encodeJson($result,0));
            } else {
                $message = Helper::getErrorMessage('IncorrectData');
                exit(Helper::encodeJson($message,1));
            }
        }
    }

    public function userAction() {
        $this->view->header = 'Доступы пользователей';
        $this->view->title = 'Simple Application';

        if(isset($this->request['isAjax']) && isset($this->request['page']) ) {

            if( !preg_match('/^\D+$/', $this->request['page']) && $this->request['page']) {
                $result = $this->defaultModel->getUsersByPageId($this->request['page']);
                exit(Helper::encodeJson($result,4));
            } else {
                $message = Helper::getErrorMessage('IncorrectData');
                exit(Helper::encodeJson($message,1));
            }
        }
    }

    public function pageAction() {
        $this->view->header = 'Поиск по заголовкам и ключевым словам';
        $this->view->title = 'Simple Application';

        if(isset($this->request['isAjax']) && isset($this->request['page']) && isset($this->request['query'])  ) {
            if($this->request['page'] === $this->request['query']) {
                $result = "“Бугага, смищное совпадение”";
                exit(Helper::encodeJson($result,0));
            }
            if( !preg_match('/^\D+$/', $this->request['page']) && $this->request['page'] && $this->request['query']) {
                $result = $this->defaultModel->getTitleAndKeyByPageId($this->request['page'],$this->request['query']);
                if(is_array($result)) {
                    exit(Helper::encodeJson($result,4));
                } else {
                    $message = Helper::getErrorMessage('IsEmptyData');
                    exit(Helper::encodeJson($message,1));
                }
            }
            $message = Helper::getErrorMessage('IncorrectData');
            exit(Helper::encodeJson($message,1));
        }
    }

    public function rssAction() {
        $result = $this->defaultModel->getRss();
        if($result) {
            exit(Helper::encodeJson($result));
        }
        exit();
    }

}