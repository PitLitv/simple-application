<?php
Config::set('routes', ['default' => '']);

Config::set('default_route', 'default');
Config::set('default_controller', 'default');
Config::set('default_action', 'index');

Config::set('db.host', 'localhost' );
Config::set('db.user', 'postgres' );
Config::set('db.password', '' );
Config::set('db.db_name', 'postgres' );

Config::set('RSA_CONFIG', [
    'digest_alg'=>'sha512',
    'private_key_bits'=>'4096',
    'private_key_type'=>'OPENSSL_KEYTYPE_RSA',
] );

Config::set('PUBLIC_KEY', APPLICATION . 'config' . DS . 'RSA_PUBLIC_KEY' );
Config::set('PRIVATE_KEY', APPLICATION . 'config' . DS . 'RSA_PRIVATE_KEY' );