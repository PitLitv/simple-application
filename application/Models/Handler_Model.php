<?php

class Handler_Model
{

    public function dateFormat($date) {
        $time = strtotime($date);
        if($time === false)
            return false;

        $months = [
            'Январь' => 'Января',
            'Февраль' => 'Февраля',
            'Март' => 'Марта',
            'Апрель' => 'Апреля',
            'Май' => 'Мая',
            'Июнь' => 'Июня',
            'Июль' => 'Июля',
            'Август' => 'Августа',
            'Сентябрь' => 'Сентября',
            'Октябрь' => 'Октября',
            'Ноябрь' => 'Ноября',
            'Декабрь' => 'Декабря'
        ];
        $locale = setlocale (LC_TIME, 'ru_RU.UTF-8', 'Rus');
        $date = strftime("%a, %d %B %G {year}, %R", $time);

        if (strpos($locale, '1251') !== false) {
            $date = iconv('cp1251', 'utf-8', $date);
        }
        $date =  str_replace("{year}", "года", $date);
        return strtr($date,$months);
    }

    public function compileCardNumber($array) {
        $result = false;
        if(isset($array['one']) &&  preg_match('/^\d{4}$/', $array['one']) &&
            isset($array['two']) && preg_match('/^\d{4}$/', $array['two']) &&
            isset($array['three']) && preg_match('/^\d{4}$/', $array['three']) &&
            isset($array['four']) && preg_match('/^\d{4}$/', $array['four'])) {
            $result = "Номер карты: ".implode(' - ',$array);
        }
        return $result;
    }

    public function convertRsaToArray($encrypted) {
        $json = Helper::decodeRsa($encrypted);
        if($jsonArray = Helper::decodeJson($json,true)) {
            $jsonArray = current($jsonArray);
            arsort($jsonArray);
        }
        return $jsonArray;
    }

    public function exponentiation($value, $count, $current, $actions = []) {
        $actions[] = $current ;
        if($count > 1) {
            return $this::exponentiation($value * $current, $count - 1, $current, $actions);
        } elseif($count == 1 && $value > $current) {
            return "Результат: $value  </br> Действия: ".implode(' * ',$actions);
        } elseif($count == 1 && $value != 1 && $value == $current) {
            return "Результат: $value  </br> Действия: $current * 1";
        } else {
            return "Результат: 1 ";
        }
    }

    public function validateEmail($email) {
        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $result = true;
        } else {
            $result = false;
        }
        return $result;
    }
}
