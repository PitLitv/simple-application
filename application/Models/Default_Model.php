<?php

class Default_Model extends Model
{
    public function __construct() {
        parent::__construct();
    }

    public function getUsersByPageId($id)
    {
        $id = (int)$id;
        $sql = '
            SELECT
                "us"."id",
                "us"."name",
                "us"."email",
                "us"."phone",
                "gr"."id" AS group_id,
                "gr"."name" AS group_name,
                "acs"."page_id" AS access_is_granted
            FROM
                "public"."groups" AS "gr"
            INNER JOIN "public"."users_groups_relations" AS "us_gr" ON "gr"."id" = "us_gr"."group_id"
            INNER JOIN "public"."users" AS "us" ON "us"."id" = "us_gr"."user_id"
            LEFT JOIN "public"."access" AS "acs" ON "acs"."group_id" = "gr"."id" AND "acs"."page_id" = ' . $id  . '
            ORDER BY "us"."id"
        ';
        $result = $this->db->query($sql);
        return $result ? $result : null;
    }

    public function getTitleAndKeyByPageId($id,$query)
    {

        $query = "'$query'";
        $id = (int)$id;
        $sql = '
            SELECT
                    "art"."id",
                    "art"."title",              
                    "art"."keywords",              
                    \'articles\' AS collection               
            FROM "public"."articles" AS "art"
                    INNER JOIN "public"."relations_pages_news_articles_reviews" AS "relation"  ON "art"."id" = "relation"."article_id" AND ("art"."title" = ' . $query  . ' OR "art"."keywords"  = ' . $query  . ')
            WHERE "relation"."page_id" = ' . $id  . '
            UNION ALL
            SELECT
                    "rew"."id",
                    "rew"."title",           
                    "rew"."keywords",           
                    \'reviews\' AS collection               
            FROM "public"."reviews" AS "rew"
                    INNER JOIN "public"."relations_pages_news_articles_reviews" AS "relation"  ON "rew"."id" = "relation"."review_id" AND ("rew"."title" = ' . $query  . ' OR "rew"."keywords"  = ' . $query  . ')
            WHERE "relation"."page_id" = ' . $id  . ' 
            UNION ALL
            SELECT
                    "new"."id",
                    "new"."title",             
                    "new"."keywords",             
                    \'news\' AS collection               
            FROM "public"."news" AS "new"
                    INNER JOIN "public"."relations_pages_news_articles_reviews" AS "relation"  ON "new"."id" = "relation"."news_id" AND ("new"."title" = ' . $query  . ' OR "new"."keywords"  = ' . $query  . ')
            WHERE "relation"."page_id" = ' . $id  . ' 
          ';
        //2Helper::debug($sql,0,1);
        $result = $this->db->query($sql);
        return $result ? $result : null;
    }

    public function getRSS()
    {
        $sql = '
			SELECT
			    "new"."title",
			    "new"."description",
			    "new"."publish_date"
            FROM
                "public"."news"  AS "new"
            Order BY "new"."publish_date"
            LIMIT 5
            ';
        $result = $this->db->query($sql);
        return $result ? $result : null;
    }

}